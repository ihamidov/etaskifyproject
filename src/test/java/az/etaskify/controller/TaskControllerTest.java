package az.etaskify.controller;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import az.etaskify.domain.Authority;
import az.etaskify.domain.Role;
import az.etaskify.domain.Task;
import az.etaskify.domain.User;
import az.etaskify.dto.TaskDto;
import az.etaskify.dto.UserDTO;
import az.etaskify.service.TaskService;
import az.etaskify.service.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@RunWith(SpringRunner.class)
@WebMvcTest(TaskController.class)
public class TaskControllerTest {
    private static final String GETTASK_API = "/gettask/";
    private static final String CREATETASK_API = "/createtask/";

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private TaskService taskService;
    @MockBean
    private UserService userService;

    @Autowired
    private ObjectMapper objectMapper;
    private TaskDto taskDto;
    private Task task;
    private User user;
    private UserDTO userDTO;
    List<User> list=new ArrayList();

    @Before
    public void setUp() {
        Set<String> autoritys  = new HashSet<>();
        autoritys.add("admin");
        autoritys.add("user");
        Authority authority = new Authority();
        authority.setName(Role.ROLE_ADMIN);
        Authority authority2 = new Authority();
        authority2.setName(Role.ROLE_USER);
        Set<Authority> autority  = new HashSet<>();
        autority.add(authority);
        autority.add(authority2);
        user = User.builder()
                .email("testmail@mail.ru")
                .username("testuser").authorities(autority)
                .name("testName")
                .surname("testSurname")
                .password("testpass").build();

        UserDTO userDTO = UserDTO.builder()
                .email("testmail@mail.ru")
                .username("testuser")
                .name("testName")
                .surname("testSurname")
                .password("testpass")
                .matchingPassword("testpass")
                .authority(autoritys).build();
        list.add(user);
    }

    @Test
    public void getTasksByListIsOk() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(GETTASK_API))
                .andExpect(status().isOk());
    }


    private String asJsonString(final Object obj) throws JsonProcessingException {
        return objectMapper.writeValueAsString(obj);
    }
}

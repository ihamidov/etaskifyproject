package az.etaskify;

import az.etaskify.domain.Authority;
import az.etaskify.domain.Role;
import az.etaskify.domain.User;
import az.etaskify.dto.UserDTO;
import az.etaskify.repository.AuthorityRepository;
import az.etaskify.service.UserService;
import java.util.HashSet;
import java.util.Set;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class ETaskifyApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(ETaskifyApplication.class, args);
		AuthorityRepository repository = context.getBean(AuthorityRepository.class);
		UserService userService = context.getBean(UserService.class);

		Authority authority = new Authority();
		authority.setName(Role.ROLE_ADMIN);
		Authority authority2 = new Authority();
		authority2.setName(Role.ROLE_USER);
		repository.save(authority);
		repository.save(authority2);

		Set<String> autoritys  = new HashSet<>();
		autoritys.add("admin");
		autoritys.add("user");
		UserDTO userDTO = UserDTO.builder()
				.email("testmail@mail.ru")
				.username("testuser")
				.name("testName")
				.surname("testSurname")
				.password("testpass")
				.matchingPassword("testpass")
				.authority(autoritys).build();

		userService.registrationUser(userDTO);
	}

}

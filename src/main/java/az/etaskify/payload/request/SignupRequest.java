package az.etaskify.payload.request;


import az.etaskify.dto.RoleDTO;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

@Data
public class SignupRequest {

    @NotBlank
    @Size(min = 3, max = 20)
    private String username;

    @NotBlank
    @Size(max = 50)
    @Email
    private String email;

    private List<RoleDTO> role;

    @NotBlank
    @Size(min = 6, max = 40)
    private String password;

}

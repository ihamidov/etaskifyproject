package az.etaskify.config;


import java.util.ArrayList;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    public Docket swagConfig() {
        return new Docket(DocumentationType.SWAGGER_2).select()
                .apis(RequestHandlerSelectors.basePackage("com.etaskify.controller"))
                .build()
                .apiInfo(getApiInfo());
    }

    private ApiInfo getApiInfo() {
        return new ApiInfo("etaskify rest api",
                "Documentation for etaskify","1.0",
                "Terms of service for using etaskify",
                new Contact("Ibrahim Hamidov",
                        "gitlab.com/ibrahimhamidov",
                        "ibrahim.hamidov@mail.ru"),
                "Mit Lisence",
                "https://opensource.org/licenses/MIT",
                new ArrayList<>()
        );
    }


}

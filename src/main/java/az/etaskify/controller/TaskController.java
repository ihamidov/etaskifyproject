package az.etaskify.controller;

import az.etaskify.dto.TaskDto;
import az.etaskify.service.TaskService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/task")
@RequiredArgsConstructor
public class TaskController {

    private final TaskService taskService;

//    @PreAuthorize("hasRole('ROLE_USER')")
    @PostMapping("/createtask")
    public void createTask(@RequestBody TaskDto taskDto) {
        taskService.save(taskDto);
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @GetMapping("/gettask")
    public List<TaskDto> getTasks() {
        return taskService.getTasks();
    }
}
